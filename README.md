# Overview

[![pipeline status](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/-/commits/main)
[![coverage report](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/-/commits/main)
[![Latest Release](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/-/badges/release.svg)](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/-/releases)
[![Go Report Card](https://goreportcard.com/badge/gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys)](https://goreportcard.com/report/gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys)

Look up SSH Public Keys from an LDAP Service! You can use this either from your
SSH Server, or just the command line.

## Installation

Download binaries from the [release page](https://gitlab.oit.duke.edu/devil-ops/golang-ldap-ssh-keys/-/releases)

You can also pull from the normal devil-ops locations...check out the
[installing-devil-ops-packages](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
for more info

## Usage

On the command line, you can run with:

```shell
$ ldap-ssh-key-lookup some-netid
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/9vhMdBYzVqWtN6jo/jYNje0wyZ7ukBOfFPcfJdY7i some-netid@workstation
```

To use this with OpenSSH, add the following to your `sshd_config` file:

```text
AuthorizedKeysCommand /usr/bin/ldap-ssh-key-lookup
AuthorizedKeysCommandUser nobody
```

If you have SELinux set to enforcing, allow the lookup by running:

```bash
$ sudo setsebool -P authlogin_nsswitch_use_ldap 1
```

### Configuration

This is meant to be a pretty simple app to just retrieve and print keys if they
exist, so there's not a config. If you would like to change the ldap server
or other options from the defaults, you can do so with the following:

```shell
export SSHKEY_LDAP_URL=ldaps://something.else.edu:636
export SSHKEY_BASEDN="dn=example.com"
```

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
