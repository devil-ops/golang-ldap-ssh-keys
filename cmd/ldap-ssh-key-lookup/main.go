/*
Package main is the CLI app for LDAP lookups
*/
package main

import (
	"fmt"
	"io"
	"log/slog"
	"net/url"
	"os"
)

// Execute exec the CLI app
func Execute(w io.Writer) error {
	if len(os.Args) != 2 {
		return fmt.Errorf("usage: %s netid", os.Args[0])
	}

	c, cerr := New()
	if cerr != nil {
		return cerr
	}
	defer dclose(c)

	lookupKey(w, c, os.Args[1])
	return nil
}

func lookupKey(w io.Writer, ldc *Client, user string) {
	// Actual key strings
	ret := ldc.FetchKeys(user)

	// Did we just get an empty response? Causes here are likely missing or
	// bad ssh keys
	if ret == "" {
		slog.Debug("no keys found", "user", os.Args[1])
	}

	// Print keys to stdout
	fmt.Fprintln(w, ret)
}

func main() {
	if err := Execute(os.Stdout); err != nil {
		panic(err)
	}
}

// dclose Check error even on defer'd closes
func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		fmt.Fprintf(os.Stderr, "error closing item\n")
	}
}

func validateURL(s string) error {
	_, err := url.Parse(s)
	if err != nil {
		return err
	}
	return nil
}
