package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/go-ldap/ldap/v3"
)

var (
	ml ldap.Client
	c  *Client
)

func setup() {
	ml = &mockLDAP{}
	c = MustNew(
		WithLDAPClient(ml),
		WithBaseDN("dn=example.com"),
	)
}

func teardown() {
	dclose(ml)
	dclose(c)
}

type mockLDAP struct{}

func (m *mockLDAP) Close() error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) GetLastError() error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) DirSync(*ldap.SearchRequest, int64, int64, []byte) (*ldap.SearchResult, error) {
	return nil, errors.New("not yet implemented")
}

func (m *mockLDAP) SearchAsync(context.Context, *ldap.SearchRequest, int) ldap.Response {
	return nil
}

func (m *mockLDAP) Syncrepl(context.Context, *ldap.SearchRequest, int, ldap.ControlSyncRequestMode, []byte, bool) ldap.Response {
	return nil
}

func (m *mockLDAP) DirSyncAsync(context.Context, *ldap.SearchRequest, int, int64, int64, []byte) ldap.Response {
	return nil
}

func (m *mockLDAP) Start() {
}

func (m *mockLDAP) StartTLS(*tls.Config) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Stop() error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Bind(username, password string) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Unbind() error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Add(*ldap.AddRequest) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Del(*ldap.DelRequest) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Modify(*ldap.ModifyRequest) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) Compare(dn, attribute, value string) (bool, error) {
	return true, nil
}

func (m *mockLDAP) NTLMUnauthenticatedBind(domain, username string) error {
	return errors.New("not yet implemented")
}

func (m *mockLDAP) TLSConnectionState() (tls.ConnectionState, bool) {
	return tls.ConnectionState{}, false
}

func (m *mockLDAP) Search(sr *ldap.SearchRequest) (*ldap.SearchResult, error) {
	filterKeys := map[string][][]byte{
		"(uid=foo)":      {[]byte("ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/9vhMdBYzVqWtN6jo/jYNje0wyZ7ukBOfFPcfJdY7i foo@bar")},
		"(uid=emptykey)": {[]byte("")},
	}
	var k [][]byte
	if val, ok := filterKeys[sr.Filter]; ok {
		k = val
	} else {
		return nil, errors.New("This filter has not been mocked")
	}
	return &ldap.SearchResult{
		Entries: []*ldap.Entry{
			{
				DN: "cn=foo,dc=example,dc=com",
				Attributes: []*ldap.EntryAttribute{
					{
						Name:       "sshPublicKey",
						ByteValues: k,
					},
				},
			},
		},
	}, nil
}

func (m *mockLDAP) ExternalBind() error {
	return nil
}

func (m *mockLDAP) IsClosing() bool {
	return false
}

func (m *mockLDAP) ModifyDN(*ldap.ModifyDNRequest) error {
	return nil
}

func (m *mockLDAP) ModifyWithResult(*ldap.ModifyRequest) (*ldap.ModifyResult, error) {
	return nil, nil
}

func (m *mockLDAP) PasswordModify(*ldap.PasswordModifyRequest) (*ldap.PasswordModifyResult, error) {
	return nil, nil
}

func (m *mockLDAP) SearchWithPaging(sr *ldap.SearchRequest, pagingSize uint32) (*ldap.SearchResult, error) {
	return nil, nil
}

func (m *mockLDAP) SetTimeout(time.Duration) {
}

func (m *mockLDAP) SimpleBind(*ldap.SimpleBindRequest) (*ldap.SimpleBindResult, error) {
	return nil, nil
}

func (m *mockLDAP) UnauthenticatedBind(username string) error {
	return nil
}

func TestExtractKeysFromEntries(t *testing.T) {
	tests := []struct {
		entries [][]byte
		want    []string
		msg     string
	}{
		{
			entries: [][]byte{
				[]byte(
					"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjxHavxeRGpTYsoEoTRzJzUobuk4kRMJwOYPWNYYqLtC3WKJL5wB8piJgttJ46+wALJV+oOeZ/XqYOfCLl38KyrEJbqTdYRv1aGoIIM5eZM+VBmsCQqGzFcVnwAio7BDNvU+ocU8lbS32dGvUD++LIYYYZQ5U57wOqMBz8PKW/zSmKdhjzh/mlmbFC3kgJNF8TBnyJgrsGf5201RnmsfNMD8+fUwUkmiMWEvqZktnAzpo1ljm//+RK8wMEG66Onp2Kp+IP1fqLUmxq3S8j7SbJ8qlPA4myEvVWeSFJvzDyZPrTpepa0HZfGHLOshFUXZLU8v36xZhYPVvzDRUwZidz foo@bar",
				),
			},
			want: []string{
				"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjxHavxeRGpTYsoEoTRzJzUobuk4kRMJwOYPWNYYqLtC3WKJL5wB8piJgttJ46+wALJV+oOeZ/XqYOfCLl38KyrEJbqTdYRv1aGoIIM5eZM+VBmsCQqGzFcVnwAio7BDNvU+ocU8lbS32dGvUD++LIYYYZQ5U57wOqMBz8PKW/zSmKdhjzh/mlmbFC3kgJNF8TBnyJgrsGf5201RnmsfNMD8+fUwUkmiMWEvqZktnAzpo1ljm//+RK8wMEG66Onp2Kp+IP1fqLUmxq3S8j7SbJ8qlPA4myEvVWeSFJvzDyZPrTpepa0HZfGHLOshFUXZLU8v36xZhYPVvzDRUwZidz foo@bar",
			},
			msg: "Single entry RSA key with comment",
		},
		{
			entries: [][]byte{
				[]byte(
					"what is an ssh key?",
				),
			},
			want: []string{""},
			msg:  "Single entry with invalid key",
		},
	}

	for _, tt := range tests {
		got := strings.Split(string(extractKeys([]*ldap.Entry{
			{
				Attributes: []*ldap.EntryAttribute{
					{
						Name:       "sshPublicKey",
						ByteValues: tt.entries,
					},
				},
			},
		})), "\n")
		if !testEq(tt.want, got) {
			t.Errorf(`%v:
expected:
%v

got:
%v`, tt.msg, tt.want, got)
		}
	}
}

func TestValidatePubkey(t *testing.T) {
	tests := []struct {
		key       string
		expectErr string
	}{
		{key: "foo", expectErr: "ssh: no key found"},
		{key: "", expectErr: "ssh: no key found"},
		{key: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/9vhMdBYzVqWtN6jo/jYNje0wyZ7ukBOfFPcfJdY7i foo@work-laptop"},
		{key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCjxHavxeRGpTYsoEoTRzJzUobuk4kRMJwOYPWNYYqLtC3WKJL5wB8piJgttJ46+wALJV+oOeZ/XqYOfCLl38KyrEJbqTdYRv1aGoIIM5eZM+VBmsCQqGzFcVnwAio7BDNvU+ocU8lbS32dGvUD++LIYYYZQ5U57wOqMBz8PKW/zSmKdhjzh/mlmbFC3kgJNF8TBnyJgrsGf5201RnmsfNMD8+fUwUkmiMWEvqZktnAzpo1ljm//+RK8wMEG66Onp2Kp+IP1fqLUmxq3S8j7SbJ8qlPA4myEvVWeSFJvzDyZPrTpepa0HZfGHLOshFUXZLU8v36xZhYPVvzDRUwZidz foo@bar"},
	}
	for _, tt := range tests {
		err := validatePubKey([]byte(tt.key))
		if tt.expectErr != "" {
			if err.Error() != tt.expectErr {
				t.Errorf("expected error: %v, but got %v", tt.expectErr, err)
			}
		} else {
			if err != nil {
				t.Errorf("no error expected, but got: %v", err)
			}
		}
	}
}

func TestDefaultNew(t *testing.T) {
	_, err := New()
	if err != nil {
		t.Errorf("got error when none was expected: %v\n", err)
	}
}

func TestNoLDAPNew(t *testing.T) {
	_, err := New(WithLDAPURL(""))
	if got := testErr(err, "if ldapClient is not set, you must set ldapURL"); got != "" {
		t.Error(got)
	}
}

func TestInvalidLDAPNew(t *testing.T) {
	_, err := New(WithLDAPURL(":foo:"))
	if got := testErr(err, `LDAP Result Code 200 "Network Error": parse ":foo:": missing protocol scheme`); got != "" {
		t.Error(got)
	}
}

func testErr(err error, want string) string {
	if want != "" && err == nil {
		return "expected nill error, but got nil"
	}
	if err.Error() != want {
		return fmt.Sprintf("expected %v, but got %v", want, err.Error())
	}
	return ""
}

func testEq(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestValidURL(t *testing.T) {
	if err := validateURL("ldaps://example.com:636"); err != nil {
		t.Errorf("got unexpected error while validating URL: %v", err)
	}
}

func TestInValidURL(t *testing.T) {
	if got := testErr(validateURL(":foo:"), "parse \":foo:\": missing protocol scheme"); got != "" {
		t.Error(got)
	}
}

func TestFetchKeys(t *testing.T) {
	setup()
	defer teardown()

	tests := map[string]struct {
		given  string
		expect string
	}{
		"success": {
			given:  "foo",
			expect: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/9vhMdBYzVqWtN6jo/jYNje0wyZ7ukBOfFPcfJdY7i foo@bar",
		},
		"empty": {
			given:  "emptyKey",
			expect: "",
		},
	}
	for desc, tt := range tests {
		got := c.FetchKeys(tt.given)
		if got != tt.expect {
			t.Errorf("%v: expected %v, but got %v", desc, tt.expect, got)
		}
	}
}

func TestLookupKeys(t *testing.T) {
	setup()
	defer teardown()

	tests := map[string]struct {
		given  string
		expect string
	}{
		"good": {
			given:  "foo",
			expect: "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF/9vhMdBYzVqWtN6jo/jYNje0wyZ7ukBOfFPcfJdY7i foo@bar\n",
		},
		"bad": {
			given:  "never-exists",
			expect: "\n",
		},
	}
	for desc, tt := range tests {
		b := bytes.NewBufferString("")
		lookupKey(b, c, tt.given)
		if tt.expect != b.String() {
			t.Errorf(`%v:
expected:
%v

got:
%v
`, desc, tt.expect, b.String())
		}
	}
}
