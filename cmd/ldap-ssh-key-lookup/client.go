package main

import (
	"bytes"
	"cmp"
	"errors"
	"fmt"
	"log/slog"
	"os"
	"strings"

	"github.com/go-ldap/ldap/v3"
	"golang.org/x/crypto/ssh"
)

// Client is the thing we use to do the actual lookups
type Client struct {
	ldapClient  ldap.Client
	baseDN      string
	ldapURL     string
	filterTempl string
	attributes  []string
}

// Option are functional options to instantiate a new client
type Option func(*Client)

// WithLDAPURL sets the ldap URL in a new client
func WithLDAPURL(s string) Option {
	return func(c *Client) {
		c.ldapURL = s
	}
}

// WithLDAPClient sets the ldap client for a lookup client
func WithLDAPClient(l ldap.Client) Option {
	return func(c *Client) {
		c.ldapClient = l
	}
}

// WithBaseDN sets the base DN for the lookups
func WithBaseDN(s string) Option {
	return func(c *Client) {
		c.baseDN = s
	}
}

// MustNew returns a new client for lookups using functional options or panics if an error happens
func MustNew(opts ...Option) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new client for lookups using functional options
func New(opts ...Option) (*Client, error) {
	c := &Client{
		filterTempl: `(uid=%s)`,
		attributes:  []string{"sshPublicKey"},
		ldapURL:     cmp.Or(os.Getenv("SSHKEY_LDAP_URL"), "ldaps://idms-authdir.oit.duke.edu:636"),
		baseDN:      cmp.Or(os.Getenv("SSHKEY_LDAP_DN"), "dc=duke,dc=edu"),
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.ldapClient == nil {
		if c.ldapURL == "" {
			return nil, errors.New("if ldapClient is not set, you must set ldapURL")
		}
		var err error
		if c.ldapClient, err = ldap.DialURL(c.ldapURL); err != nil {
			return nil, err
		}
	}
	return c, nil
}

// Close closes the ldap connection
func (kl *Client) Close() error {
	return kl.ldapClient.Close()
}

// FetchKeys returns keys for a given user as a string
func (kl Client) FetchKeys(user string) string {
	keysB, err := kl.fetchKeyBytes(user)
	if err != nil {
		return ""
	}
	return strings.Trim(string(keysB), "\n")
}

// fetchKeyBytes returns keys for a given user
func (kl *Client) fetchKeyBytes(user string) ([]byte, error) {
	entries, err := kl.ldapSearch(user)
	if err != nil {
		return nil, err
	}

	var keys []byte
	if len(entries) > 0 {
		keys = extractKeys(entries)
	}
	return bytes.Trim(keys, "\n"), nil
}

func (kl *Client) ldapSearch(user string) ([]*ldap.Entry, error) {
	searchReq := ldap.NewSearchRequest(
		kl.baseDN,
		ldap.ScopeWholeSubtree,
		0, 0, 0,
		false,
		fmt.Sprintf(kl.filterTempl, ldap.EscapeFilter(user)),
		kl.attributes,
		[]ldap.Control{},
	)
	result, err := kl.ldapClient.Search(searchReq)
	if err != nil {
		return nil, err
	}

	return result.Entries, nil
}

func extractKeys(entries []*ldap.Entry) []byte {
	var buffer bytes.Buffer
	for _, entry := range entries {
		for _, attr := range entry.Attributes {
			for _, key := range attr.ByteValues {
				if err := validatePubKey(key); err != nil {
					slog.Warn("could not parse key", "key", string(key))
				} else {
					buffer.Write(key)
					buffer.Write([]byte("\n"))
				}
			}
		}
	}
	return bytes.Trim(buffer.Bytes(), "\n")
}

// validatePubKey just makes sure the pubkey is an actual public key,
// and not a random set of strings someone has entered
func validatePubKey(pubKey []byte) error {
	if _, _, _, _, err := ssh.ParseAuthorizedKey(pubKey); err != nil {
		return err
	}
	return nil
}
